<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('weight');
            $table->string('course_type')->nullable(); 
            $table->integer('course_id')->nullable();
            $table->integer('max_allowed_time_seconds')->nullable();  
            $table->timestamps();
        });
        Schema::create('questions', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('quiz_id');
            $table->text('content'); // the question text
            $table->text('file');
            // image or text or audio
            $table->string('choices_type');
            $table->integer('correct_choice_id');
            $table->integer('weight');
            $table->integer('question_order');
            $table->string('question_type'); // Radio, Checklist
            $table->timestamps();
        });
        Schema::create('question_choices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id');
            $table->text('content'); //text 
            $table->text('file');
            // path of image/audio
            $table->integer('correct')->default(0);
            $table->integer('stuff_order'); // if the choices orders are selected by the user and not automatically randomized by the system
            $table->timestamps();
        });
        Schema::create('quiz_enteries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('quiz_id');
            $table->integer('user_id');
            $table->boolean('finished')->default(0); // the user clicked finish or not
            $table->float('grade');
            $table->datetime('start_time')->nullable();
            $table->datetime('end_time')->nullable();
            $table->timestamps();
        });
        Schema::create('question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entry_id');
            $table->integer('user_id');
            $table->integer('question_id');
            $table->integer('quiz_id');
            $table->string('choices_ids');
            $table->float('degree');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quizes');
        Schema::drop('questions');
        Schema::drop('question_choices');
        Schema::drop('quiz_enteries');
        Schema::drop('question_answers');
    }
}
