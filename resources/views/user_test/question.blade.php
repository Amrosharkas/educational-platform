<div class="row">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td align="center">Time Remaining</td>
        </tr>
        <tr>
            <td align="center">  
                <?php
                $end_time = date("Y-m-d H:i:s", strtotime($entry->start_time . ' +30 minutes'));

                date_default_timezone_set("Africa/Cairo");
                $current_time = strtotime(date("Y-m-d H:i:s"));
                if ($current_time > strtotime($end_time)) {
                    $secounds_remaining = 1;
                } else {
                    $secounds_remaining = strtotime($end_time) - $current_time;
                }
                ?>

                <div class="timer" style="direction:ltr;" data-seconds-left="<?php echo $secounds_remaining;?>"></div>
            </td>
        </tr>
    </table>
</div>
<div class="row">
	<div style="padding-left:30px;">
    <form class="ajax_form" action="/admin/user_test/answerQuestion" method="post">
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="question_id" value="{{$question->id}}">
        <h3>{{$question->content}}</h3>
        <div class="form-group form-md-checkboxes">
            <div class="md-checkbox-list">
        @if($question->question_type == "Checklist")    
        @foreach($question->getAnswers as $answer)
                <div class="md-checkbox">
                    <input id="checkbox{{$answer->id}}" name="answers[]" class="md-check" type="checkbox" value="{{$answer->id}}">
                    <label for="checkbox{{$answer->id}}">
                        <span class="inc"></span>
                        <span class="check"></span>
                        <span class="box"></span> {{$answer->content}} </label>
                </div>
        @endforeach
        @endif
        @if($question->question_type == "Radio")    
        @foreach($question->getAnswers as $answer)
                <div class="md-radio">
                    <input id="radio{{$answer->id}}" name="answers" class="md-radio" type="radio" value="{{$answer->id}}">
                    <label for="radio{{$answer->id}}">
                        <span></span>
                        <span class="check"></span>
                        <span class="box"></span> {{$answer->content}} </label>
                </div>
        @endforeach
        @endif
            </div>
        </div>
        <button type="submit" class="btn btn-primary submit_answer" >Submit answer</button>
    </form>
	</div>
</div>
<div class="row">
<div style="padding-left:30px; padding-top:30px;">
Question <span class="bold text-success"><?php echo $question->question_order+1;?></span> of <span class="bold text-info">{{$total}}</span> 
</div>
</div>