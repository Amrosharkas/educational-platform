<div style="padding-left:30px; font-size:20px;">
<p>Note:</p>
<p>You have only one attempt at the test please make sure that you have a working internet connection and avoid any distractions. The test is composed of 20 multiple choice questions and should be completed in a maximum of 30 minutes. </p>
<p>To start the test <a class="pjax-link" href="/admin/user_test/startTest/{{Auth::user()->id}}">click here</a></p>
</div>