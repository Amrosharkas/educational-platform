<div class="portlet box grey-cascade">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-globe"></i>Materials
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse">
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-toolbar">
            <div class="row">
                <div class="col-md-6">
                    <div class="btn-group">
                                             
                    </div>
                </div>
                <div class="col-md-6"></div>
            </div>
        </div>
        <table class="table table-striped table-bordered table-hover table-dt" >
            <thead>
                <tr class="tr-head">
                    <th valign="middle">
                        Title
                    </th>
                    <th valign="middle">
                        Action
                    </th>
                </tr>
            </thead>
            <tbody>
                @foreach($materials as $material)
                <tr class="odd gradeX" id="data-row-{{$material->id}}">
                    <td valign="middle">
                        {{$material->name}}
                    </td>
                    <td valign="middle">
                    <?php if($material->type == "File" && $material->content_type == "HTML5"){?><a href="{{route('admin.user_material.view',['id'=>$material->id])}}" class="btn green pjax-link" ><i class="fa fa-eye"></i> View</a><?php ;}?>
                    <?php if($material->type == "Video"){?><a href="https://www.youtube.com/watch?v={{$material->youtube_id}}" class="popup btn green" ><i class="fa fa-youtube"></i> Watch Video</a><?php ;}?>
                    <?php if($material->type == "File" && $material->content_type == "Other"){?><a href="{{route('admin.user_material.material_file',['id'=>$material->id])}}" class="btn green" ><i class="fa fa-download"></i> Download Material</a><?php ;}?>
                    </td>
                </tr>
                @endforeach
            <tr></tbody>
        </table>
    </div>
</div>