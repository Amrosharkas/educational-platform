<?php $i = "material"; $j = "material"; ?>
@extends('admin.master')
@section('plugins_css')
<link href="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/datatables.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/sweetAlert/sweetalert.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.css')}}"/>
@stop

@section('plugins_js')
<script type="text/javascript" src="{{asset('assets/global/scripts/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/datetimepicker/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jsvalidation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/sweetAlert/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/global/plugins/jquery-nestable/jquery.nestable.js')}}"></script>
@stop

@section('page_js')
<script>
var pageAttributes = {
    indexUrl: "",
}

</script>
<script type="text/javascript" src="{{asset('assets/ajaxForms.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/scripts.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/admin/pages/scripts/material.js')}}"></script>
@endSection

@section('add_inits')

@stop

@section('title')
Training Materials
@stop

@section('page_title')
Training Materials
@stop

@section('page_title_small')

@stop

@section('content')
@include($partialView)
@stop

