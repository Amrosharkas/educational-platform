<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>@if($material)Edit @else New @endif </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                
                                                <div class="form-group">
                    <label>Type </label>
                    <select name="material_type1" class="form-control material_type1">
                    	<option value="" >Select Material Type</option>
                        
                        <option value="Training"  <?php if($material->material_type == "Training"){?> selected="selected"<?php ;}?>>Training Material</option>
                        <option value="Course" <?php if($material->material_type == "Course"){?> selected="selected"<?php ;}?>>Course Material</option>
                    </select>
                </div>
                <div class="form-group courseType <?php if($material->material_type != "Course"){?>hide<?php ;}?>">
                <div class="row">
                <div class="col-md-8">
                    <label>Course </label>
                    <select name="course_id" class="form-control editable-select">
                    	
                        @foreach($courses as $course)
                        <option value="{{$course->id}}"  <?php if($material->course_id == $course->id){?> selected="selected"<?php ;}?>>{{$course->name}}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-md-4">
                    
                    <button type="button" class="btn btn-sm red <?php if($material->course_id == ""){?>hide<?php ;}?>"  id="delete-course" style="margin-top:27px;" >Delete</button>
                    </div>
                </div>
                </div>
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="material_name" class="form-control" value="{{$material->name}}">
                </div>
                <div class="form-group">
                    <label>Order</label>
                    <input name="material_order" class="form-control" value="{{$material->material_order}}">
                </div>
                <div class="form-group">
                    <label>Type</label>
                    <select name="material_type" class="form-control material_type">
                    	<option value="" >Select Type</option>
                        
                        <option value="File"  <?php if($material->type == "File"){?> selected="selected"<?php ;}?>>File Or HTML5</option>
                        <option value="Video" <?php if($material->type == "Video"){?> selected="selected"<?php ;}?>>Youtube Video</option>
                    </select>
                </div>
                
                <div class="<?php if($material->type !="Video"){?>hide<?php ;}?> form-group youtube">
                    <label>Youtube ID</label>
                    <input name="youtube_id" class="form-control" value="{{$material->youtube_id}}">
                </div>
                <div class="<?php if($material->type !="File"){?>hide<?php ;}?> form-group fileUploading">
                    <label >Attach File</label>
                    <input type="file" class="hidden fileinput"
                           data-action="{{route('admin.material.upload_material_file',['id'=>$material->id])}}" 
                           >
                    <div class="row">
                        <div class="col-md-8">
                            <span class="filenameplaceholder">
                                @if($material->content)
                                <a class="btn btn-primary filename" href="{{route('admin.material.material_file',['id'=>$material->id])}}" >
                                  <i class="fa fa-edit"></i>  Download File
                                </a>
                                @else
                                No file selected
                                @endif
                            </span>
                            <div class="progress-bar progress-bar-success hidden" role="progressbar"  style="width: 0%">
                                <span>0%</span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary filepicker pull-right {{$material->content?'hidden':''}}">
                                Select File
                            </button>
                            <button class="btn btn-danger pull-right cancel hidden">Cancel</button>
                            <button class="btn btn-danger pull-right removefile {{$material->content?'':'hidden'}}" 
                                    data-action="{{route('admin.material.delete_material_file',['id'=>$material->id])}}"
                                    >
                                Detach file
                            </button>
                        </div>
                        
                    </div>
                </div>
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg green" id="save-all" data-action="{{route('admin.material.update',['id'=>$material->id])}}">Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')