<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> New  </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <div  id="main-form" novalidate method="POST" class="larajsval form"> 
            <div class="form-body">
            <div class="mt-element-ribbon bg-grey-steel">
                                                <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-primary uppercase">
                                                    <div class="ribbon-sub ribbon-clip"></div> Main Info </div>
                                                <p class="ribbon-content">
                                                <div class="form-group">
                                                  <div class="form-group">
                    <label>Name</label>
                    <input name="name" class="form-control" >
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input name="email" type="email" class="form-control" >
                </div>
                <div class="form-group">
                    <label>mobile</label>
                    <input name="mobile" class="form-control" >
                </div>
                <div class="form-group">
                    <label>password</label>
                    <input name="password" type="password" class="form-control" >
                </div>
                
                
                
                
                </p>
                
                                            </div>
                                            
                                            </div>
            

            </div>
            <div class="form-actions">
                <div class="btn-set pull-right">
                    <button type="button" class="btn btn-lg green" id="save-all" data-action="{{route('admin.candidates.save')}}">Save</button>
                </div>
            </div>
        </div>
        <!-- END FORM-->
    </div>
</div>
@include('quizzes.templates')