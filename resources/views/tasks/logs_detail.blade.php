<div class="row">
<div class="col-sm-6">
<h3 style="text-align:left; margin-top:0;">{{$task->name}} Logs</h3>
</div>
<div class="col-sm-6 hide">
<input name="task-status" @if($task->status == "Finished") disabled checked @endif type="checkbox"  class="make-switch pull-right" data-on-color="success" data-off-color="info" data-on-text="Finished" data-off-text="Running">
</div>
</div>
<table class="table table-striped table-bordered table-hover table-dt"  >
                <thead>
                    <tr class="tr-head">
                        <th valign="middle">
                            Start time
                        </th>
                        <th valign="middle">
                            End time
                        </th>
                        <th valign="middle">User</th>
                        <th valign="middle">By</th>
                        <th valign="middle">
                            Created at
                        </th>
                        <th valign="middle">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($logs as $log)
                    <tr class="odd gradeX" id="data-row-{{$log->id}}">
                        <td valign="middle">
                            {{ Carbon\Carbon::parse($log->start_time)->format('D d, M Y H:i a') }}
                        </td>
                        <td valign="middle">
                        @if($log->end_time){{ Carbon\Carbon::parse($log->end_time)->format('D d, M Y H:i a') }}@endif
                        @if(!$log->end_time) Null @endif    
                        </td>
                        <td valign="middle">{{$log->getUser->name}}</td>
                        <td valign="middle">{{$log->getAddedBy->name}}</td>
                        <td valign="middle">
                            
                            {{ Carbon\Carbon::parse($log->created_at)->format('D d, M Y H:i a') }}
                        </td>
                        <td valign="middle">
                        @if(($task->status != "Finished" && $log->added_by_id == Auth::user()->id && $log->end_time) || Auth::user()->user_type == "Allocator")
                            <a href="{{route('admin.tasks.delete_log',['id'=>$log->id])}}" class="btn red remove-course" ><i class="fa fa-remove"></i></a> 
                        @endif    
                        </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            @if(Auth::user()->user_type == "Allocator")
<form class="ajax_form" method="post" action="/admin/tasks/addLog">
        <div class="row">
            <div class="col-sm-5">
                <div class="input-group">
                    <span class="input-group-btn">
                         <button class="btn blue" type="button"><i class="fa fa-clock-o"></i> Start Time</button>
                    </span>
                  <input class="form-control timepicker" type="text" name="start_time"> </div>
                <!-- /input-group -->
            </div>
            <div class="col-sm-2">
            <button type="submit" class="btn blue center-block" >Add log</button>
            </div>
          <!-- /.col-md-6 -->
            <div class="col-sm-5">
                <div class="input-group">
                  <input class="form-control timepicker" type="text" name="end_time">
                    <span class="input-group-btn">
                         <button class="btn red" type="button">End Time <i class="fa fa-clock-o"></i></button>
                    </span>
                </div>
                <!-- /input-group -->
            </div>
            <!-- /.col-md-6 -->
        </div>
        
  
        <input type="hidden" name="task_id" id="task_id" value="{{$task->id}}" />
        <input type="hidden" name="user_id" id="user_id" value="{{$task->getUser->id}}"/>
        
        </form>
        @endif