$(document).on('click', '#add_new', function () {
    var url = $(this).data('action');
    $.get(url).success(function (data) {
        pjaxPage(data.url)
    });
});







$(document).on('click', '#save-all', function () {

    var url = $(this).data('action');
    var data = {};
    var $mainForm = $('#main-form');
    data['name'] = $mainForm.find('[name=name]').val();
    data['email'] = $mainForm.find('[name=email]').val();
    data['mobile'] = $mainForm.find('[name=mobile]').val();
    data['password'] = $mainForm.find('[name=password]').val();
    if(data['name'] == ""){
        toastr['error']('Please insert a name', "Sorry");
        return false;
    }
    if(data['email'] == ""){
        toastr['error']('Please insert an Email', "Sorry");
        return false;
    }
    if(data['mobile'] == ""){
        toastr['error']('Please insert a mobile', "Sorry");
        return false;
    }
    if(!validateEmail(data['email'])){
        toastr['error']('Invalid Email', "Sorry");
        return false;
    }
    
    if(data['password'] == ""){
        toastr['error']('Please insert a password', "Sorry");
        return false;
    }
App.blockUI({
            boxed: true
        });


    $.ajax({
        method: 'patch',
        url: url,
        data: data,
        success: function (data) {
            toastr['success']('User Added successfuly ', "Done");
            // pjaxPage(data.url);
             App.unblockUI();

        },
        error: function (data) {
            toastr['error']('There was an error ', "Error");
             App.unblockUI();

        }
    });
});
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
$(document).on('click', '#save-all-edit', function () {

    var url = $(this).data('action');
    var data = {};
    var $mainForm = $('#main-form');
    data['id'] = $mainForm.find('[name=id]').val();
    data['name'] = $mainForm.find('[name=name]').val();
    data['email'] = $mainForm.find('[name=email]').val();
    data['mobile'] = $mainForm.find('[name=mobile]').val();
    data['user_phase'] = $mainForm.find('[name=user_phase]').val();
    data['user_status'] = $mainForm.find('[name=user_status]').val();
    data['quiz'] = $mainForm.find('[name=quiz]').val();
    data['fluency'] = $mainForm.find('[name=fluency]').val();
    data['grammar'] = $mainForm.find('[name=grammar]').val();
    data['accent'] = $mainForm.find('[name=accent]').val();
    data['vocab'] = $mainForm.find('[name=vocab]').val();
    data['pilot'] = $mainForm.find('[name=pilot]').val();
    
    if(data['name'] == ""){
        toastr['error']('Please insert a name', "Sorry");
        return false;
    }
    if(data['email'] == ""){
        toastr['error']('Please insert an Email', "Sorry");
        return false;
    }
    if(!validateEmail(data['email'])){
        toastr['error']('Invalid Email', "Sorry");
        return false;
    }
    
    if(data['password'] == ""){
        toastr['error']('Please insert a password', "Sorry");
        return false;
    }
    App.blockUI({
            boxed: true
        });


    $.ajax({
        method: 'patch',
        url: url,
        data: data,
        success: function (datas) {
            toastr['success']('User Edited successfuly ', "Done");
            conn.sendMsg("test_result","haw",data['id']);
            App.unblockUI();
            swal(
              'Done!',
              'Please remember if you have to notify this candidate about the change',
              'success'
            )
            
            // pjaxPage(data.url);
        },
        error: function (data) {
            App.unblockUI();

        }
    });
});


$(document).on('click', '.candidate-change-status', function (e) {
    e.preventDefault();
    
    var user_id = $(this).closest('tr').attr('data-user_id');
    
    $row = $(this).closest('tr');
    var action = $(this).attr('href');
    var user_phase = $(this).attr('data-phase');
    if (user_phase == "pilotTest") {
        swal({
            title: 'Please intsert the pilot test grade',
            input: 'text',
            showCancelButton: true,
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            preConfirm: function (grade) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        if (isNaN(grade)) {
                            reject('Please insert a valid number.')
                        } else {
                            resolve()
                        }
                    }, 2000)
                })
            },
            allowOutsideClick: false
        }).then(function (grade) {
            $.ajax({
                url: action,
                method: 'post',
                data: {
                    grade: grade,
                    gradesFor: user_phase
                },
                success: function () {
                    swal("OK!", 'Successfuly done', "success");
                    table.row($row).remove().draw();
                    conn.sendMsg("test_result","haw",user_id);
                },
                error: function () {
                    swal("Error", 'Something is wrong, Try again later', "error");
                }
            });

        })
    }

    if (user_phase == "testEvaluation") {
        swal({
            title: 'Are you sure?',
            
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes!'
        }).then(function () {
            $.ajax({
                url: action,
                method: 'post',
                data: {

                    gradesFor: user_phase
                },
                success: function () {
                    swal("Ok!", 'Successfuly done', "success");
                    table.row($row).remove().draw();
                    conn.sendMsg("test_result","haw",user_id);
                },
                error: function () {
                    swal("Error", 'Something is wrong, Try again later', "error");
                }
            });
        })
        
    }

    if (user_phase == "phoneInterview") {
        swal.setDefaults({
            input: 'text',
            confirmButtonText: 'Next &rarr;',
            showCancelButton: true,
            animation: false,
            progressSteps: ['1', '2', '3', '4']
        })

        var steps = [
            {
                title: 'Fluency Grade',
                inputValue: 0
        },
            {
                title: 'Vocabluary Grade',
                inputValue: 0
        },
            {
                title: 'Grammar Grade',
                inputValue: 0
        },
            {
                title: 'Accent Grade',
                inputValue: 0
        }
]

        swal.queue(steps).then(function (result) {
            swal.resetDefaults()
            $.ajax({
                url: action,
                method: 'post',
                data: {
                    answers: JSON.stringify(result),
                    gradesFor: user_phase
                },
                success: function () {
                    swal("OK!", 'Done Successfuly', "success");
                    conn.sendMsg("test_result","haw",user_id);
                    table.row($row).remove().draw();
                },
                error: function () {
                    swal("Error", 'Something is wrong, Try again later', "error");
                }
            });

        }, function () {
            swal.resetDefaults()
        })
    }


});


