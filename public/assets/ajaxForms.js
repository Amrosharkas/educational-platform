$(document).on('submit', '.ajaxForm', function (e) {
    e.preventDefault();
    var $submitButton = $(this).find('[type=submit]');
    $submitButton.attr('disabled', 'disabled');
    var formData = $(this).serialize();
    var method = $(this).find('input[name=_method]').val();
    var url = $(this).attr('action');
    if (!method) {
        method = $(this).attr('method');
    }
    $.ajax({
        url: url,
        method: method,
        data: formData,
        dataType: 'json',
        success: function (data) {
            if (method.toUpperCase() == 'POST' && data.status == 1) {
                toastr['success'](data.message, "Done");
                document.getElementById('index-page').click();
                return;
            }
            if (method.toUpperCase() == 'PATCH' && data.status == 1) {
                toastr['success'](data.message, "Done");
                document.getElementById('index-page').click();
                return;
            }
            if (data.status == -1) {
                toastr['error'](data.message, "Error");
                $submitButton.removeAttr('disabled');
                return;
            }
        },
        error: function (data, textStatus, xhr) {
            $submitButton.removeAttr('disabled');
            if (xhr.status == 422) {
                alert('Error');
            }
        },
    })
})
