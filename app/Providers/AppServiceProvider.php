<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use View;
use Session;
use App\Course;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        date_default_timezone_set("Africa/Cairo");
        $evaluation = User::where('user_phase','testEvaluation')->where('user_type','Volunteer')->where('user_status','Pending')->count() ;
       $phone_interview = User::where('user_phase','phoneInterview')->where('user_type','Volunteer')->where('user_status','Pending')->count() ;
       $pilot_test = User::where('user_phase','pilotTest')->where('user_type','Volunteer')->where('user_status','Pending')->count() ;
       $courses = Course::orderBy('name','asc')->get();
       View::share('evaluation', $evaluation);
       View::share('phone_interview', $phone_interview);
       View::share('pilot_test', $pilot_test);
       View::share('courses', $courses);



    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {


    }
}
