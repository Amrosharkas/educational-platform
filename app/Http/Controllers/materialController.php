<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Material;
use App\File;
use ZipArchive;
use RecursiveIteratorIterator;
use FilesystemIterator;
use App\Course;

class materialController extends Controller
{
    public function deleteCourse($id){
        $course = Course::where('name',$id)->delete();
    }
    public function index() {
        $data = [];
        $data['partialView'] = 'material.list';
        $data['materials'] = Material::orderBy('material_order', 'asc')->get();
        return view('material.base', $data);
    }

    public function init() {
        $material = new Material();
        $material->save();
        return response(['url' => route('admin.material.edit', ['id' => $material->id])]);
    }

    public function edit($id) {
        $material = Material::findOrFail($id);
        $courses = Course::orderBy('name')->get();
        $data = [];
        $data['partialView'] = 'material.form';
        $data['material'] = $material;
        $data['courses'] = $courses;
        return view('material.base', $data);
    }
    public function uploadMaterialFile($id, Request $request) {
        $material = Material::findOrFail($id);
        $uploadedFile = $request->file('file');
        $dirPath = public_path('uploads/material/material_file/'.$material->id);
        $saveName = md5(microtime(true) . uniqid(str_random(3)) . str_random(10));
        $uploadedFile->move($dirPath, $saveName);
        $file = new File();
        $file->path = $dirPath . '/' . $saveName;
        $file->name = $uploadedFile->getClientOriginalName();
        $file->save();
        $ext = pathinfo($file->name, PATHINFO_EXTENSION);
        $material->content_type = "Other";
        if(strtolower($ext) == "zip"){
            $material->content_type = "HTML5";
        	$zip = new ZipArchive();
			$x = $zip->open($file->path);
			if ($x === true) {
				$zip->extractTo($dirPath); // change this to the correct site path
				$zip->close();
		
				
			}
			

        }

        $material->content = $file->id;
        $material->type = "File";
        $material->save();
        $responseData = [];
        $responseData['remove_action'] = route('admin.material.delete_material_file', ['id' => $material->id]);
        $responseData['download_action'] = route('admin.material.material_file', ['id' => $material]);
        $responseData['file_name'] = $file->name;
        return response()->json($responseData);
    }
    public function materialFile($id) {
        $material = Material::findOrFail($id);
        if ($material->materialFile()) {
            return response()->download($material->materialFile()->path, $material->materialFile()->name);
        }
        abort(404);
    }
    public function deleteMaterialFile($id) {
        Material::findOrFail($id)->deleteMaterialFile();
        $dirPath = public_path('uploads/material/material_file/'.$id);
        $this->recursiveRmDir($dirPath);
        
    }
    public function update(Request $request, $id) {
        $course = Course::where('name',$request->course_id)->first();

        if(!$course){
            $course = new Course();
            $course->name = $request->course_id;
            $course->save();
        }
        $material = Material::findOrFail($id);
        $material->name = $request->name;
        $material->material_type = $request->material_type;
        $material->type = $request->type;
        $material->material_order = $request->order;
        $material->youtube_id = $request->youtube_id;
        $material->course_id = $course->id;
        $material->save();
        
        $responseData = [];
        $responseData['url'] = "/admin/material";
        return response()->json($responseData);
    }

    public function delete($id) {
        Material::findOrFail($id)->delete();
    }

    public function recursiveRmDir($dir)
	{
	    $iterator = new RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST);
	    foreach ($iterator as $filename => $fileInfo) {
	        if ($fileInfo->isDir()) {
	            rmdir($filename);
	        } else {
	            unlink($filename);
	        }
	    }
	    rmdir($dir);
}
}
