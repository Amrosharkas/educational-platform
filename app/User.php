<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getQuizEntry(){
        return $this->hasOne('App\Quiz_entry','user_id','id');
    }

    public function getGrades(){
        return $this->hasOne('App\User_grade','user_id','id');

    }

   
}
